package main

import (
	"encoding/json"
	"fmt"
	"io"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/brakeman/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

const (
	brakemanDocURLPrefix = "https://brakemanscanner.org/docs/warning_types/"
)

var cveRegexp = regexp.MustCompile(`(CVE-\d{4}-\d{4,7})`)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var doc = struct {
		Warnings []Warning `json:"warnings"`
	}{}

	err := json.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	vulns := []report.Vulnerability{}
	for _, w := range doc.Warnings {
		// Ignore vulnerabilities affecting project dependencies
		// because these are already reported by GitLab Dependency Scanning.
		if !w.AffectsDependency() {
			vulns = append(vulns, report.Vulnerability{
				Category: metadata.Type,
				Scanner:  metadata.IssueScanner,
				// w.Message is free of contextual data when vulnerability is not affecting dependencies
				Name:        w.Message,
				Message:     w.Message,
				Confidence:  w.ConfidenceLevel(),
				Severity:    w.SeverityLevel(),
				CompareKey:  w.Fingerprint,
				Location:    w.ConvertLocation(prependPath),
				Identifiers: w.Identifiers(),
				Links:       w.Links(),
			})
		}
	}

	newReport := report.NewReport()
	newReport.Vulnerabilities = vulns
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	return &newReport, nil
}

// Warning describes a vulnerability found in the source code.
type Warning struct {
	WarningType string      `json:"warning_type"`
	WarningCode int         `json:"warning_code"`
	Fingerprint string      `json:"fingerprint"`
	CheckName   string      `json:"check_name"`
	Message     string      `json:"message"`
	File        string      `json:"file"`
	Line        int         `json:"line"`
	Link        string      `json:"link"`
	Code        string      `json:"code"`
	RenderPath  interface{} `json:"render_path"`
	Location    struct {
		Type   string `json:"type"`
		Class  string `json:"class"`
		Method string `json:"method"`
	} `json:"location"`
	UserInput  string `json:"user_input"`
	Confidence string `json:"confidence"`
}

// AffectsDependency tells whether this vulnerability affects a gem the project depends on.
func (w Warning) AffectsDependency() bool {
	return filepath.Base(w.File) == "Gemfile.lock"
}

// ConfidenceLevel returns the normalized confidence
// See https://github.com/presidentbeef/brakeman/blob/v4.3.1/lib/brakeman/warning.rb#L13-L17
func (w Warning) ConfidenceLevel() report.ConfidenceLevel {
	switch w.Confidence {
	case "High":
		return report.ConfidenceLevelHigh
	case "Medium":
		return report.ConfidenceLevelMedium
	case "Weak":
		return report.ConfidenceLevelLow
	}
	return report.ConfidenceLevelUnknown
}

// SeverityLevel returns the normalized severity
// This level is a remapping of confidence as described in the docs:
// https://github.com/presidentbeef/brakeman#confidence-levels
func (w Warning) SeverityLevel() report.SeverityLevel {
	switch w.Confidence {
	case "High":
		return report.SeverityLevelHigh
	case "Medium":
		return report.SeverityLevelMedium
	case "Weak":
		return report.SeverityLevelLow
	}
	return report.SeverityLevelUnknown
}

// Links returns the normalized links of the vulnerability.
func (w Warning) Links() []report.Link {
	// If Warning's Link points to Brakeman documentation, do not add it
	// here as it's already included in the Brakeman Identifier.
	if strings.Contains(w.Link, brakemanDocURLPrefix) || w.Link == "" {
		return []report.Link{}
	}

	return []report.Link{
		report.Link{
			URL: w.Link,
		},
	}
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (w Warning) Identifiers() []report.Identifier {
	identifiers := []report.Identifier{
		w.BrakemanIdentifier(),
	}

	// Try to find CVE-ID within Message
	cve := cveRegexp.FindString(w.Message)
	if cve != "" {
		identifiers = append(identifiers, report.CVEIdentifier(cve))
	}

	return identifiers
}

// BrakemanIdentifier returns a structured Identifier for a brakeman Warning Code
func (w Warning) BrakemanIdentifier() report.Identifier {
	var url string
	// If Link points to Brakeman documentation, use it as Identifier Url.
	if strings.Contains(w.Link, brakemanDocURLPrefix) {
		url = w.Link
	}
	// TODO: else { build url if available on https://brakemanscanner.org/docs/warning_types }

	return report.Identifier{
		Type:  "brakeman_warning_code",
		Name:  fmt.Sprintf("Brakeman Warning Code %d", w.WarningCode),
		Value: strconv.Itoa(w.WarningCode),
		URL:   url,
	}
}

// ConvertLocation returns a structured Location
func (w Warning) ConvertLocation(prependPath string) report.Location {

	location := report.Location{
		File:      filepath.Join(prependPath, w.File),
		LineStart: w.Line,
	}

	// Only take locations with "Method" type
	if w.Location.Type == "method" {
		location.Class = w.Location.Class
		location.Method = w.Location.Method
	}

	return location
}
