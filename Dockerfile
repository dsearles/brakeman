ARG VET_VERSION=0.7.2
ARG STENCILS_VERSION=0.0.6
ARG POST_ANALYZER_SCRIPTS_VERSION=0.0.2
ARG TRACKING_CALCULATOR_VERSION=2.2.1

FROM registry.gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking
FROM registry.gitlab.com/gitlab-org/security-products/vet/vet:${VET_VERSION} AS vet
FROM registry.gitlab.com/gitlab-org/security-products/vet/stencils:${STENCILS_VERSION} AS stencils
FROM registry.gitlab.com/gitlab-org/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS start
FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

RUN apk update && apk add curl

WORKDIR /go/src/buildapp
COPY . .

# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
  PATH_TO_MODULE=`go list -m` && \
  go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer-brakeman

FROM ruby:2.7-alpine

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-5.0.1}

RUN apk update
RUN apk add --no-cache git
RUN apk add musl
RUN apk upgrade

RUN gem update webrick
RUN gem install brakeman -v $SCANNER_VERSION

COPY --from=build /analyzer-brakeman /analyzer-binary
COPY --from=tracking /analyzer-tracking /analyzer-tracking
COPY --from=vet /usr/bin/analyzer /vet
COPY --from=stencils /stencils /stencils
COPY --from=start /start.sh /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
